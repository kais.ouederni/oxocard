# Oxocard - A way to make computer science attractive to a young audience

![MCU](imgCarteDeDev/cardsOxocard.png){: style="height:150px;width:150px"}

Bienvenue aux ateliers oxocard

## URL VERS LE SITE
    https://oxocard-kais-ouederni-295e470e66f3ac7302da28675d71b6c9bf2ed215f.pages.forge.hefr.ch/

<!-- 
## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

## 
-->