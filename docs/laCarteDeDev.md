# La carte de développement

## L'UNITE DE CONTROLLE PRINCIPAL

![MCU](imgCarteDeDev/mainControlUnit.png){ align=left }


## ECRAN

![DISPLAY](imgCarteDeDev/display.png){ align=left }

## DETECTEURS

![SENSORS](imgCarteDeDev/sensors.png){ align=left }

## USB 

![USB](imgCarteDeDev/usb.png){ align=left }